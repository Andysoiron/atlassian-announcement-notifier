# Atlassian announcement notifier

This is a script to create GitLab issues whenever a new announcement appears on the [Atlassian developer community](https://community.developer.atlassian.com/c/announcements/22)

The script is supposed to run in a daily scheduled pipeline on GitLab.
