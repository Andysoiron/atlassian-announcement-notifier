# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'rss'
require_relative 'atlassian_announcement_notifier/gitlab_api'

URL = 'https://community.developer.atlassian.com/c/announcements/22.rss'

gitlab = GitlabApi.new('https://gitlab.com/api/v4', ENV['GITLAB_PROJECT'], ENV['GITLAB_TOKEN'])
yesterday = Time.now.utc.to_date() -1
rss_content = HTTParty.get(URL).body

RSS::Parser.parse(rss_content).items.each do |item|
  next unless item.pubDate.to_date > yesterday

  gitlab.create_issue(item) if ENV['CREATE_ISSUES']
end
