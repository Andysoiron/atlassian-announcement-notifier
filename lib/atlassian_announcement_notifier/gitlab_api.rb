# frozen_string_literal: true

require 'gitlab'

class GitlabApi
  def initialize(endpoint, project, private_token)
    @project = project
    ::Gitlab.endpoint = endpoint
    ::Gitlab.private_token = private_token
  end

  def create_issue(item)
    ::Gitlab.create_issue(
      @project,
      item.title,
      description: description(item),
      labels: 'group::integrations,Backlog Refinement::Integrations'
    )
  end

  private

  def description(item)
    "A new [Atlassian developer community announcement](#{item.link}) was published. Please evaluate if an action is required."
  end
end